#!/usr/bin/env python3

import rclpy
from rclpy.node import Node

from std_msgs.msg import Float64MultiArray
from sensor_msgs.msg import JointState


class EPuckController(Node):
    def __init__(self):
        super().__init__('epuck_controller')


        self.cmd_publisher_ = self.create_publisher(
            Float64MultiArray, '/wheels_velocity_controller/commands', 10)

        self.joint_state_subscription_ = self.create_subscription(
            JointState,
            '/joint_states',
            self.joint_state_callback,
            10)

        self.cmd_ = Float64MultiArray(data=[0, 0])

        self.start_time_ = self.get_clock().now().seconds_nanoseconds()[0]
        self.run_timer_ = self.create_timer(0.1, self.run)




    def run(self):
        if self.get_clock().now().seconds_nanoseconds()[0] > self.start_time_ + 2.0:
            self.cmd_.data = [0.5, 0.5]
        else:
            self.cmd_.data = [0.0, 0.0]

        self.cmd_publisher_.publish(self.cmd_)






    def joint_state_callback(self, msg):
        self.get_logger().info(
            "\nWheels\n\tname: %s\n\tposition: %s\n\tvelocity: %s" %
            (msg.name, msg.position, msg.velocity))


def main(args=None):

    rclpy.init(args=args)

    epuck_controller = EPuckController()

    try:
        rclpy.spin(epuck_controller)
    finally:
        rclpy.shutdown()


if __name__ == '__main__':
    main()
