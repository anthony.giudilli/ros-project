#!/usr/bin/env python3

import numpy as np

import math
from math import *

import rclpy
from rclpy.node import Node

from std_msgs.msg import Float64MultiArray
from sensor_msgs.msg import JointState
from sensor_msgs.msg import Range

def normalizeAngle(angle):
    while angle >= np.pi:
        angle -= 2.0 * np.pi
    while angle < -np.pi:
        angle += 2.0 * np.pi
    return angle 

class EPuckController(Node):
    def __init__(self):
        super().__init__('epuck_controller')

        self.L = 0.052 / 2
        self.r = 0.0205

        self.theta = -np.pi / 2
        self.x = 0
        self.y = 0

        self.pLeft = 0
        self.pRight = 0

        self.declare_parameter('x')
        self.declare_parameter('y')

        self.xDes = self.get_parameter('x').value 
        self.yDes = self.get_parameter('y').value


        self.cmd_publisher_ = self.create_publisher(
            Float64MultiArray, '/wheels_velocity_controller/commands', 10)

        self.joint_state_subscription_ = self.create_subscription(
            JointState,
            '/joint_states',
            self.joint_state_callback,
            10)
        

        self.start_time_ = self.get_clock().now().seconds_nanoseconds()[0]
        self.run_timer_ = self.create_timer(0.1, self.run)



    def run(self):


        cmd = Float64MultiArray(data=[0, 0])

        dx = self.xDes - self.x
        dy = self.yDes - self.y

        if abs(dx) > 0.05 or abs(dy) > 0.05 :

            dl = sqrt(dx ** 2 + dy ** 2 );

            dTh = normalizeAngle( atan2(dy, dx) - normalizeAngle(self.theta) )   

            v = 5 * dl
            w = 10 * dTh

            vd = v + self.L * w
            vg = v - self.L * w   

            cmd.data = [vg, vd]



        self.cmd_publisher_.publish(cmd)



    def joint_state_callback(self, msg):


        dLeft = msg.position[0] * self.r - self.pLeft
        dRight = msg.position[1] * self.r - self.pRight

        dCenter = (dLeft + dRight) / 2
        phi = (dRight - dLeft) / (2 * self.L)

        self.theta = self.theta + phi
        self.x = self.x + dCenter * math.cos(self.theta)
        self.y = self.y + dCenter * math.sin(self.theta)

        self.pLeft = msg.position[0] * self.r
        self.pRight = msg.position[1] * self.r


        self.get_logger().info(
            "\nx %s y %s theta %s" %
            (self.x, self.y, self.theta))





def main(args=None):
    rclpy.init(args=args)

    epuck_controller = EPuckController()

    try:
        rclpy.spin(epuck_controller)
    finally:
        rclpy.shutdown()


if __name__ == '__main__':
    main()
