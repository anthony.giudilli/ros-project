#include <memory>
#include <iostream>

#include <rclcpp/rclcpp.hpp>
#include <std_msgs/msg/float64_multi_array.hpp>
#include <sensor_msgs/msg/joint_state.hpp>

#include <fmt/format.h>

using JointState = sensor_msgs::msg::JointState;
using Float64MultiArray = std_msgs::msg::Float64MultiArray;

class EPuckController : public rclcpp::Node {
public:
    EPuckController() : Node("epuck_controller") {
        cmd_publisher_ = create_publisher<Float64MultiArray>(
            "/wheels_velocity_controller/commands", 10);

        joint_state_subscription_ = create_subscription<JointState>(
            "/joint_states", 10,
            [this](JointState::SharedPtr msg) { joint_state_callback(msg); });

        cmd_.data.resize(2, 0);

        start_time_ = get_clock()->now().seconds();

        using namespace std::chrono_literals;
        run_timer_ = create_wall_timer(100ms, [this] { run(); });
    }

    void run() {
        if (get_clock()->now().seconds() < start_time_ + 2) {
            cmd_.data = {1.0, -1.0};
        } else {
            cmd_.data = {0.0, 0.0};
        }
        cmd_publisher_->publish(cmd_);

    }

private:
    void joint_state_callback(const JointState::SharedPtr msg) const {
        RCLCPP_INFO(
            get_logger(),
            fmt::format(
                "\nWheels\n\tname: {}\n\tposition: {}\n\tvelocity: {}\n",
                fmt::join(msg->name, ", "), fmt::join(msg->position, ", "),
                fmt::join(msg->velocity, ", ")));
    }

    rclcpp::Subscription<JointState>::SharedPtr joint_state_subscription_;
    rclcpp::Publisher<Float64MultiArray>::SharedPtr cmd_publisher_;
    rclcpp::TimerBase::SharedPtr run_timer_;

    Float64MultiArray cmd_;
    double start_time_{};
};

int main(int argc, char* argv[]) {
    rclcpp::init(argc, argv);

    auto epuck_controller = std::make_shared<EPuckController>();

    rclcpp::spin(epuck_controller);
    rclcpp::shutdown();

    return 0;
}
