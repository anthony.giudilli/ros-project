from launch import LaunchDescription
from ament_index_python.packages import get_package_share_directory
from launch.actions import IncludeLaunchDescription
from launch.actions import DeclareLaunchArgument
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.substitutions import LaunchConfiguration
from launch.substitutions import TextSubstitution
import os
import launch_ros.actions

def generate_launch_description():


    x_launch_arg = DeclareLaunchArgument(
        "x", default_value=TextSubstitution(text="0")
    )
    y_launch_arg = DeclareLaunchArgument(
        "y", default_value=TextSubstitution(text="0")
    )


    pkg_share = os.path.join(
        get_package_share_directory('epuck_ros2'))

    model = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(
            os.path.join(pkg_share, 'launch', 'model.launch.py'))
    )

    gazebo = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(
            os.path.join(pkg_share, 'launch', 'gazebo.launch.py'))
    )

    return LaunchDescription([
        model,
        gazebo,
        x_launch_arg,
        y_launch_arg,
        launch_ros.actions.Node(
            #package='epuck_ros2', executable='script.py', parameters=[{"y" : LaunchConfiguration('y')}])
            package='epuck_ros2', executable='script.py', parameters=[{"x" : LaunchConfiguration('x'), "y" : LaunchConfiguration('y')}])
    ])


