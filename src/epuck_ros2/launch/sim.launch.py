from launch import LaunchDescription
from ament_index_python.packages import get_package_share_directory
from launch.actions import IncludeLaunchDescription
from launch.launch_description_sources import PythonLaunchDescriptionSource
import os
import launch_ros.actions

def generate_launch_description():
    pkg_share = os.path.join(
        get_package_share_directory('epuck_ros2'))

    model = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(
            os.path.join(pkg_share, 'launch', 'model.launch.py'))
    )

    gazebo = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(
            os.path.join(pkg_share, 'launch', 'gazebo.launch.py'))
    )

    return LaunchDescription([
        model,
        gazebo,
        launch_ros.actions.Node(
            package='epuck_ros2', executable='py_example.py')
    ])
